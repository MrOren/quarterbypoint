import './App.css';
import {useState} from "react";
import service from "./api";

function App() {
  const [firstNum, setFirstNumber] = useState(null);
  const [secondNum, setSecondNumber] = useState(null);
  const [quarter, setQuarter] = useState(null);

  const calcQuarterOnClick = () => {
    service.QuartersService.getQuarters(firstNum,secondNum)
        .then(response => {
            setQuarter(response.quarter)

        })
  }

  return (
      <div className={'app'}>
        <input placeholder={'X:'} onChange={event => setFirstNumber(event.target.value)}/>
        <input placeholder={'Y:'} onChange={event => setSecondNumber(event.target.value)}/>
        <button onClick={calcQuarterOnClick}>Click to show quarter</button>
        <div>The answer is: </div>
        <span className={'quarter'}>{quarter}</span>
      </div>
  );
}

export default App;
