from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from quarterByPoint_server.models import Quarters
from quarterByPoint_server.serializers import QuartersSerializer


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def calc_quarter(request):
    firstNum = int(request.GET.get('firstNumber', ''))
    secondNum = int(request.GET.get('secondNumber', ''))
    calculated_quarter = get_quarter(firstNum, secondNum)
    quarter_obj = Quarters(first=firstNum, second=secondNum, quarter=calculated_quarter)
    quarter_obj.save()
    serializer = QuartersSerializer(quarter_obj)
    return Response(serializer.data, status=status.HTTP_200_OK)


def get_quarter(firstNum,secondNum):
    if firstNum > 0 and secondNum > 0:
        result = "First qurater (+,+)"
    elif firstNum > 0 and secondNum < 0:
        result = "Second qurater (+,-)"
    elif firstNum < 0 and secondNum < 0:
        result = "Third qurater (-,-)"
    elif firstNum < 0 and secondNum > 0:
        result = "Fourth qurater (-,+)"

    return result
