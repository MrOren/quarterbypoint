from rest_framework import serializers

from quarterByPoint_server.models import Quarters


class QuartersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Quarters
        fields = ['id', 'first', 'second', 'quarter', 'creation_date']
