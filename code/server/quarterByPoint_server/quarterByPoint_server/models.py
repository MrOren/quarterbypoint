from django.db import models


class Quarters(models.Model):
    id = models.AutoField(primary_key=True)
    first = models.IntegerField(default=0)
    second = models.IntegerField(default=0)
    quarter = models.CharField(max_length=40)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.quarter
