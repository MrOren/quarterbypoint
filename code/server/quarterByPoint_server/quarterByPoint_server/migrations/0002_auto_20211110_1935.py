# Generated by Django 3.2.9 on 2021-11-10 17:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('quarterByPoint_server', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Quarters',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('quarter', models.CharField(max_length=25)),
                ('creation_date', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.DeleteModel(
            name='Colors',
        ),
    ]
